//
//  Item.swift
//  Todoey
//
//  Created by Jesse Frederick on 3/2/23.
//

import Foundation

class Item: Codable {
    var title: String = ""
    var done: Bool = false
}
